﻿using AutoMapper;
using MediatR;

namespace NucleoCompartilhado.Aplicacao.ModelosDeVisualizacao.Servicos
{
    public abstract class ServicoDeAplicacao
    {
        protected readonly IMapper _mapeador;
        protected readonly IMediator _mediador;

        public ServicoDeAplicacao(IMapper mapeador, IMediator mediador)
        {
            this._mapeador = mapeador;
            this._mediador = mediador;
        }
    }
}
