﻿using NucleoCompartilhado.Infra.Utils.Criptografia;
using Xunit;

namespace NucleoCompartilhado.Infra.Utils.TestesDeUnidade.Criptografia
{
    public   class CriptoMD5Teste
    {
        [Fact]
        public void Deve_criptografar_corretamente_um_texto_para_MD5()
        {
            var senhaEsperada = "e10adc3949ba59abbe56e057f20f883e";

            var senhaCriptografada = CriptoMD5.TextoParaMD5("123456");

            Assert.Equal(senhaEsperada, senhaCriptografada);
        }
    }
}
