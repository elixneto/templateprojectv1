﻿using ContasDeUsuario.Dominio.Contas;
using Xunit;

namespace ContasDeUsuario.Dominio.TestesDeUnidade.Contas
{
    public class ContaTeste
    {
        [Fact]
        public void Nao_deve_criar_uma_conta_sem_nome()
        {
            var novaConta = new Conta(null);

            Assert.False(novaConta.EhValido);
        }

        [Theory]
        [InlineData("aaa")]
        [InlineData("MyAcc")]
        [InlineData("aaaaaaaaaaaaaaa")]
        [InlineData("Acênto")]
        [InlineData("Com Espaço")]
        public void Deve_criar_uma_conta(string nome)
        {
            var novaConta = new Conta(nome);

            Assert.True(novaConta.EhValido);
        }

        [Theory]
        [InlineData("a")]
        [InlineData("AA")]
        [InlineData("aaaaaaaaaaaaaaaa")]
        public void Nao_deve_criar_uma_conta_com_o_nome_invalido(string nome)
        {
            var novaConta = new Conta(nome);

            Assert.False(novaConta.EhValido);
        }
    }
}
