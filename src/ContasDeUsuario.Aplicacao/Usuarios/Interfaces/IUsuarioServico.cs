﻿using ContasDeUsuario.Aplicacao.Usuarios.ModelosDeVisualizacao;
using System.Threading.Tasks;

namespace ContasDeUsuario.Aplicacao.Usuarios.Interfaces
{
    public interface IUsuarioServico
    {
        Task<string> Criar(CriarUsuarioMV criarUsuarioMV);
    }
}
