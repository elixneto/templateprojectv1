﻿using ContasDeUsuario.Aplicacao.Usuarios.ModelosDeVisualizacao;
using ContasDeUsuario.Dominio.Usuarios;
using ContasDeUsuario.Dominio.Usuarios.Comandos;
using NucleoCompartilhado.Aplicacao.PerfisDeMapeamento;

namespace ContasDeUsuario.Aplicacao.Usuarios
{
    public class UsuarioPerfil : PerfilDeMapeamento
    {
        public UsuarioPerfil()
        {
            CreateMap<CriarUsuarioMV, CriarUsuarioComando>();
            CreateMap<Usuario, UsuarioMV>()
                .ForMember(m => m.Login, f => f.MapFrom(from => from.Login.Descricao))
                .ForMember(m => m.Situacao, f => f.MapFrom(from => from.Situacao.Descricao))
                .ForMember(m => m.EhAdmin, f => f.MapFrom(from => from.EhAdministrador))
                .ForMember(m => m.Email, f => f.MapFrom(from => from.Email.Endereco));
        }
    }
}
