﻿using AutoMapper;
using ContasDeUsuario.Aplicacao.Usuarios.Interfaces;
using ContasDeUsuario.Aplicacao.Usuarios.ModelosDeVisualizacao;
using ContasDeUsuario.Dominio.Usuarios.Comandos;
using MediatR;
using NucleoCompartilhado.Aplicacao.ModelosDeVisualizacao.Servicos;
using System.Threading.Tasks;

namespace ContasDeUsuario.Aplicacao.Usuarios
{
    public class UsuarioServico : ServicoDeAplicacao, IUsuarioServico
    {
        public UsuarioServico(IMapper mapeador, IMediator mediador) : base(mapeador, mediador)
        {
        }

        public async Task<string> Criar(CriarUsuarioMV criarUsuarioMV)
        {
            var comandoParaCriarNovoUsuario = _mapeador.Map<CriarUsuarioComando>(criarUsuarioMV);
            var novoUsuario = await _mediador.Send(comandoParaCriarNovoUsuario);

            return novoUsuario;
        }
    }
}
