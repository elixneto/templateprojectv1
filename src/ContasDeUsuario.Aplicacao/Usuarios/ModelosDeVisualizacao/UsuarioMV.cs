﻿using NucleoCompartilhado.Aplicacao.ModelosDeVisualizacao;

namespace ContasDeUsuario.Aplicacao.Usuarios.ModelosDeVisualizacao
{
    public class UsuarioMV : ModeloDeVisualizacao
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public bool EhAdmin { get; set; }
        public string Situacao { get; set; }
    }
}
