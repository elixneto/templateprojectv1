﻿using NucleoCompartilhado.Aplicacao.ModelosDeVisualizacao;
using System;
using System.Collections.Generic;

namespace ContasDeUsuario.Aplicacao.Usuarios.ModelosDeVisualizacao
{
    public class CriarUsuarioMV : ModeloDeVisualizacao
    {
        public string Conta { get; set; }
        public string Nome { get; set; }
        public bool EhAdmin { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }
        public DateTime DataDeNascimento { get; set; }
        public List<string> Permissoes { get; set; }
    }
}
