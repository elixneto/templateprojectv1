﻿using AutoMapper;
using ContasDeUsuario.Aplicacao.Contas.Interfaces;
using ContasDeUsuario.Aplicacao.Contas.ModelosDeVisualizacao;
using ContasDeUsuario.Aplicacao.Usuarios.Interfaces;
using ContasDeUsuario.Dominio.Contas.Comandos;
using MediatR;
using NucleoCompartilhado.Aplicacao.ModelosDeVisualizacao.Servicos;
using System.Threading.Tasks;

namespace ContasDeUsuario.Aplicacao.Contas
{
    public class ContaServico : ServicoDeAplicacao, IContaServico
    {
        private readonly IUsuarioServico _usuarioServico;

        public ContaServico(IMapper mapeador, IMediator mediador, IUsuarioServico usuarioServico) : base(mapeador, mediador)
        {
            this._usuarioServico = usuarioServico;
        }

        public async Task<string> Criar(CriarContaMV criarContaMV)
        {
            var comandoParaCriarNovaConta = _mapeador.Map<CriarContaComando>(criarContaMV);
            var novaConta = await _mediador.Send(comandoParaCriarNovaConta);

            if (criarContaMV.UsuarioAdmin != null)
            {
                criarContaMV.UsuarioAdmin.Conta = novaConta;
                criarContaMV.UsuarioAdmin.EhAdmin = true;

                if (!string.IsNullOrEmpty(novaConta))
                    await _usuarioServico.Criar(criarContaMV.UsuarioAdmin);
            }

            return novaConta;
        }
    }
}
