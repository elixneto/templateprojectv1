﻿using ContasDeUsuario.Aplicacao.Contas.ModelosDeVisualizacao;
using System.Collections.Generic;

namespace ContasDeUsuario.Aplicacao.Contas.Interfaces
{
    public interface IContaConsulta
    {
        List<ContaMV> ObterLista(ushort pagina, ushort quantidadeDeRegistros);
        ContaMV ObterPorId(string id);
    }
}
