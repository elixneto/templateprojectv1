﻿using ContasDeUsuario.Aplicacao.Contas.ModelosDeVisualizacao;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContasDeUsuario.Aplicacao.Contas.Interfaces
{
    public interface IContaServico
    {
        Task<string> Criar(CriarContaMV criarContaMV);
    }
}
