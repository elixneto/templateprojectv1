﻿using ContasDeUsuario.Aplicacao.Usuarios.ModelosDeVisualizacao;
using NucleoCompartilhado.Aplicacao.ModelosDeVisualizacao;

namespace ContasDeUsuario.Aplicacao.Contas.ModelosDeVisualizacao
{
    public class CriarContaMV : ModeloDeVisualizacao
    {
        public string Nome { get; set; }
        public CriarUsuarioMV UsuarioAdmin { get; set; }
    }
}
