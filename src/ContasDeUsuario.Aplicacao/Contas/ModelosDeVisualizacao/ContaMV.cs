﻿using ContasDeUsuario.Aplicacao.Usuarios.ModelosDeVisualizacao;
using NucleoCompartilhado.Aplicacao.ModelosDeVisualizacao;
using System.Collections.Generic;

namespace ContasDeUsuario.Aplicacao.Contas.ModelosDeVisualizacao
{
    public class ContaMV : ModeloDeVisualizacao
    { 
        public string Id { get; set; }
        public string Nome { get; set; }
        public List<UsuarioMV> Usuarios { get; set; }

        public ContaMV()
        {
            this.Usuarios = new List<UsuarioMV>();
        }
    }
}
