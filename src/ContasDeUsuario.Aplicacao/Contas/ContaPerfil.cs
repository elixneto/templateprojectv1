﻿using ContasDeUsuario.Aplicacao.Contas.ModelosDeVisualizacao;
using ContasDeUsuario.Dominio.Contas;
using ContasDeUsuario.Dominio.Contas.Comandos;
using NucleoCompartilhado.Aplicacao.PerfisDeMapeamento;

namespace ContasDeUsuario.Aplicacao.Contas
{
    public class ContaPerfil : PerfilDeMapeamento
    {
        public ContaPerfil()
        {
            CreateMap<CriarContaMV, CriarContaComando>();
            CreateMap<Conta, ContaMV>()
                .ForMember(m => m.Usuarios, f => f.Ignore());
        }
    }
}
