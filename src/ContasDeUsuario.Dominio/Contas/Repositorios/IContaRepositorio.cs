﻿using NucleoCompartilhado.Infra.Repositorio;

namespace ContasDeUsuario.Dominio.Contas.Repositorios
{
    public interface IContaRepositorio : IRepositorio<Conta>
    {
        Conta ObterPorNome(string nome);
    }
}
