﻿using ContasDeUsuario.Dominio.Contas.Comandos;
using ContasDeUsuario.Dominio.Contas.Eventos;
using ContasDeUsuario.Dominio.Contas.Repositorios;
using MediatR;
using NucleoCompartilhado.Dominio.Manipuladores;
using NucleoCompartilhado.Dominio.Notificacoes;

namespace ContasDeUsuario.Dominio.Contas.Manipuladores
{
    public class CriarContaManipulador : Manipulador<CriarContaComando, string>
    {
        private readonly IContaRepositorio _repositorio;

        public CriarContaManipulador(INotificador notificador, IMediator mediator, IContaRepositorio repositorio) : base(notificador, mediator)
        {
            this._repositorio = repositorio;
        }

        public override string Executar(CriarContaComando comando)
        {
            var novaConta = new Conta(comando.Nome);
            _notificador.Adicionar(novaConta);

            if (_notificador.PossuiNotificacoes)
                return string.Empty;

            _repositorio.Adicionar(novaConta);

            base.PublicarEvento(new ContaCriadaEvento(novaConta.Id, novaConta.Nome));
            return novaConta.Id;
        }

        protected override void Validar(CriarContaComando request)
        {
            var contaJaExiste = _repositorio.ObterPorNome(request.Nome) != null;
            if (contaJaExiste)
                base._notificador.Adicionar($"Já existe uma conta com o nome '{request.Nome}' cadastrada");
        }
    }
}
