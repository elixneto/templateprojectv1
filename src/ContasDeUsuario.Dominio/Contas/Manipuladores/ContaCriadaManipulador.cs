﻿using ContasDeUsuario.Dominio.Contas.Eventos;
using NucleoCompartilhado.Dominio.Eventos;
using NucleoCompartilhado.Dominio.Manipuladores;

namespace ContasDeUsuario.Dominio.Contas.Manipuladores
{
    public class ContaCriadaManipulador : ManipuladorDeEvento<ContaCriadaEvento>
    {
        public ContaCriadaManipulador(IEventoRepositorio repositorio) : base(repositorio)
        {
        }

        protected override void Executar() => base.ArmazenarEvento();
    }
}
