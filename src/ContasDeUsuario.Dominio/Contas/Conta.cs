﻿using ContasDeUsuario.Dominio.Contas.Validacoes;
using NucleoCompartilhado.Dominio.Entidades;

namespace ContasDeUsuario.Dominio.Contas
{
    public class Conta : Entidade
    {
        public string Nome { get; private set; }

        public Conta(string nome)
        {
            this.Nome = nome;

            base.Validar(this, new ContaValidacao());
        }

        public void AtualizarNome(string nome) => this.Nome = nome;
    }
}
