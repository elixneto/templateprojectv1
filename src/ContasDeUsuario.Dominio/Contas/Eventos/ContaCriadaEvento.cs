﻿using NucleoCompartilhado.Dominio.Eventos;

namespace ContasDeUsuario.Dominio.Contas.Eventos
{
    public class ContaCriadaEvento : Evento
    {
        public string ContaId { get; }
        public string ContaNome { get; }
        public override string Mensagem => $"Conta '{this.ContaNome}' criada com sucesso | ID: {this.ContaId}";

        public ContaCriadaEvento(string id, string nomeDaConta)
        {
            this.ContaId = id;
            this.ContaNome = nomeDaConta;
        }
    }
}
