﻿using NucleoCompartilhado.Dominio.Comandos;

namespace ContasDeUsuario.Dominio.Contas.Comandos
{
    public class CriarContaComando : Comando<string>
    {
        public string Nome { get; set; }
    }
}
