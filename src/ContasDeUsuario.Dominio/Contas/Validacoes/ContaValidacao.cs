﻿using FluentValidation;

namespace ContasDeUsuario.Dominio.Contas.Validacoes
{
    public class ContaValidacao : AbstractValidator<Conta>
    {
        const short MAX_CARACTERES_NOME = 15;
        const short MIN_CARACTERES_NOME = 3;

        public ContaValidacao()
        {
            RuleFor(r => r.Nome)
                .NotEmpty()
                .WithMessage(m => $"O Nome da conta deve ser informado")
                .MaximumLength(MAX_CARACTERES_NOME)
                .WithMessage(m => $"O Nome da conta não pode conter mais que {MAX_CARACTERES_NOME} caracteres")
                .MinimumLength(MIN_CARACTERES_NOME)
                .WithMessage(m => $"O Nome da conta não pode conter menos que {MIN_CARACTERES_NOME} caracteres");
        }
    }
}
