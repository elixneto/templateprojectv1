﻿using NucleoCompartilhado.Infra.Repositorio;

namespace ContasDeUsuario.Dominio.Permissoes.Repositorios
{
    public interface IFuncionalidadeRepositorio : IRepositorio<Funcionalidade>
    {

    }
}
