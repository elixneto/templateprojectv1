﻿using NucleoCompartilhado.Dominio.Entidades;
using System.Collections.Generic;
using System.Linq;

namespace ContasDeUsuario.Dominio.Permissoes
{
    public class Funcionalidade : Entidade
    {
        public string Nome { get; private set; }
        public string NomeWeb { get; private set; }
        private IList<Funcionalidade> _funcionalidades = new List<Funcionalidade>();
        public IReadOnlyCollection<Funcionalidade> Funcionalidades => _funcionalidades.ToArray();

        public Funcionalidade(string nome, string nomeWeb)
        {
            this.Nome = nome;
            this.NomeWeb = nomeWeb;
        }

        public void AdicionarFilho(Funcionalidade funcionalidadeFilho) => _funcionalidades.Add(funcionalidadeFilho);
        public void RemoverFilho(Funcionalidade funcionalidadeFilho) => _funcionalidades.Remove(funcionalidadeFilho);
    }
}
