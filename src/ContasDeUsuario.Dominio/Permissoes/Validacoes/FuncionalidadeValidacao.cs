﻿using FluentValidation;

namespace ContasDeUsuario.Dominio.Permissoes.Validacoes
{
    public class FuncionalidadeValidacao : AbstractValidator<Funcionalidade>
    {
        public FuncionalidadeValidacao()
        {
            RuleFor(r => r.Nome)
                .NotEmpty()
                .WithMessage(m => $"O Nome da funcionalidade deve ser informado");

            RuleFor(r => r.NomeWeb)
                .NotEmpty()
                .WithMessage(m => $"O Nome Web da funcionalidade deve ser informado");
        }
    }
}
