﻿using FluentValidation;

namespace ContasDeUsuario.Dominio.Permissoes.ObjetosDeValor.Validacoes
{
    public class PermissaoValidacao : AbstractValidator<Permissao>
    {
        public PermissaoValidacao()
        {
            RuleFor(r => r.Nome)
                .NotEmpty()
                .WithMessage(m => $"O Nome da funcionalidade deve ser informado");
        }
    }
}
