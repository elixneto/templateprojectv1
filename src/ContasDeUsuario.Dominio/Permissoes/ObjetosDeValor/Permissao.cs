﻿using ContasDeUsuario.Dominio.Permissoes.ObjetosDeValor.Validacoes;
using NucleoCompartilhado.Dominio.ObjetosDeValor;

namespace ContasDeUsuario.Dominio.Permissoes.ObjetosDeValor
{
    public class Permissao : ObjetoDeValor<Permissao>
    {
        public string Nome { get; private set; }

        public Permissao(string nome)
        {
            this.Nome = nome;

            base.Validar(this, new PermissaoValidacao());
        }

        public override bool PropriedadesIguais(Permissao outroObjeto)
        {
            return this.Nome == outroObjeto.Nome;
        }
    }
}
