﻿using ContasDeUsuario.Dominio.Usuarios.ObjetosDeValor;
using ContasDeUsuario.Dominio.Usuarios.Validacoes;
using NucleoCompartilhado.Dominio.Entidades;
using NucleoCompartilhado.Dominio.ObjetosDeValor;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContasDeUsuario.Dominio.Usuarios
{
    public class Usuario : Entidade
    {
        public string Conta { get; private set; }
        public string Nome { get; private set; }
        public Login Login { get; private set; }
        public Email Email { get; private set; }
        public SituacaoDoUsuario Situacao { get; private set; }
        public DateTime DataDeNascimento { get; private set; }
        public bool EhAdministrador { get; }
        private IList<string> _permissoes = new List<string>();
        public IReadOnlyCollection<string> Permissoes => _permissoes.ToArray();

        public Usuario(string conta, string nome, Login login, Email email, SituacaoDoUsuario situacao, DateTime dataDeNascimento, bool ehAdministrador)
        {
            this.Conta = conta;
            this.Nome = nome;
            this.Login = login;
            this.Email = email;
            this.Situacao = situacao;
            this.DataDeNascimento = dataDeNascimento;
            this.EhAdministrador = ehAdministrador;

            base.Validar(this, new UsuarioValidacao());
        }

        public bool PossuiPermissao(string permissao) => _permissoes.Contains(permissao);
        public void AdicionarPermissao(string permissao)
        {
            if (!this.PossuiPermissao(permissao))
                _permissoes.Add(permissao);
        }
        public void RemoverPermissao(string permissao) => _permissoes.Remove(permissao);
    }
}
