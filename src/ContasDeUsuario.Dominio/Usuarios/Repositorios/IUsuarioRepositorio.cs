﻿using NucleoCompartilhado.Infra.Repositorio;

namespace ContasDeUsuario.Dominio.Usuarios.Repositorios
{
    public interface IUsuarioRepositorio : IRepositorio<Usuario>
    {
        Usuario ObterPorLogin(string login);
    }
}
