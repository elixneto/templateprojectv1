﻿using NucleoCompartilhado.Dominio.Eventos;

namespace ContasDeUsuario.Dominio.Usuarios.Eventos
{
    public class UsuarioCriadoEvento : Evento
    {
        public string UsuarioId { get; set; }
        public string UsuarioNome { get; set; }
        public string Login { get; set; }
        public override string Mensagem => $"Usuário '{this.UsuarioNome} ({this.Login})' criado com sucesso | ID: {this.UsuarioId}";

        public UsuarioCriadoEvento(string id, string nome, string login)
        {
            this.UsuarioId = id;
            this.UsuarioNome = nome;
            this.Login = login;
        }
    }
}
