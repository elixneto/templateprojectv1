﻿using ContasDeUsuario.Dominio.Usuarios.ObjetosDeValor;

namespace ContasDeUsuario.Dominio.Usuarios.SituacoesDoUsuario
{
    public class Inativo : SituacaoDoUsuario
    {
        public override bool PermiteRealizarLogin => false;

        public Inativo() : base("Inativo") { }


        public override bool PropriedadesIguais(SituacaoDoUsuario outroObjeto)
        {
            return this.Descricao.Equals(outroObjeto.Descricao);
        }
    }
}
