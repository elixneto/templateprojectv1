﻿using ContasDeUsuario.Dominio.Usuarios.ObjetosDeValor;

namespace ContasDeUsuario.Dominio.Usuarios.SituacoesDoUsuario
{
    public class Ativo : SituacaoDoUsuario
    {
        public override bool PermiteRealizarLogin => true;

        public Ativo() : base("Ativo") { }


        public override bool PropriedadesIguais(SituacaoDoUsuario outroObjeto)
        {
            return this.Descricao.Equals(outroObjeto.Descricao);
        }
    }
}
