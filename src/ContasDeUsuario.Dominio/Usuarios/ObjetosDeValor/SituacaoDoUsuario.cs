﻿using NucleoCompartilhado.Dominio.ObjetosDeValor;

namespace ContasDeUsuario.Dominio.Usuarios.ObjetosDeValor
{
    public abstract class SituacaoDoUsuario : ObjetoDeValor<SituacaoDoUsuario>
    {
        public virtual bool PermiteRealizarLogin => false;
        public string Descricao { get; }

        public SituacaoDoUsuario(string descricao)
        {
            this.Descricao = descricao;
        }

        public virtual void Ativar()
        {
            // EXCEPTION
        }
        public virtual void Inativar()
        {

        }
    }
}
