﻿using ContasDeUsuario.Dominio.Usuarios.ObjetosDeValor.Validacoes;
using NucleoCompartilhado.Dominio.ObjetosDeValor;

namespace ContasDeUsuario.Dominio.Usuarios.ObjetosDeValor
{
    public class Login : ObjetoDeValor<Login>
    {
        public string Descricao { get; private set; }
        public string Senha { get; private set; }

        public Login(string descricao, string senha)
        {
            this.Descricao = descricao;
            this.Senha = senha;

            base.Validar(this, new LoginValidacao());
        }

        public override bool PropriedadesIguais(Login outroObjeto)
        {
            return this.Descricao == outroObjeto.Descricao &&
                this.Senha == outroObjeto.Senha;
        }
    }
}
