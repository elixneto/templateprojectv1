﻿using FluentValidation;
using System.Text.RegularExpressions;

namespace ContasDeUsuario.Dominio.Usuarios.ObjetosDeValor.Validacoes
{
    public class LoginValidacao: AbstractValidator<Login>
    {
        public LoginValidacao()
        {
            RuleFor(r => r.Descricao)
                .NotEmpty()
                .WithMessage(m => $"O Login deve ser informado")
                .MaximumLength(15)
                .WithMessage(m => $"O Login informado não pode conter mais que 15 caracteres")
                .Must(this.ContemCaracteresValidos)
                .WithMessage(m => $"O Login '{m.Descricao}' possui caracteres inválidos");
        }

        private bool ContemCaracteresValidos(string descricao)
        {
            if (string.IsNullOrEmpty(descricao)) return true;

            var regex = new Regex("^[a-zA-Z0-9_]*$", RegexOptions.None);
            return regex.IsMatch(descricao);
        }
    }
}
