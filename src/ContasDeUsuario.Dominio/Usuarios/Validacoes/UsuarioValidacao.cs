﻿using FluentValidation;

namespace ContasDeUsuario.Dominio.Usuarios.Validacoes
{
    public class UsuarioValidacao : AbstractValidator<Usuario>
    {
        const short MAX_CARACTERES = 45;
        const short MIN_CARACTERES = 3;

        public UsuarioValidacao()
        {
            RuleFor(r => r.Nome)
                .NotEmpty()
                .WithMessage(m => $"O Nome do usuário deve ser informado")
                .MinimumLength(MIN_CARACTERES)
                .WithMessage(m => $"O Nome do usuário não pode conter menos que {MIN_CARACTERES} caracteres")
                .MaximumLength(MAX_CARACTERES)
                .WithMessage(m => $"O Nome do usuário não pode conter mais que {MAX_CARACTERES} caracteres");

            RuleFor(r => r.Conta)
                .NotEmpty()
                .WithMessage(m => $"A Conta do usuário deve ser informada");

            RuleFor(r => r.DataDeNascimento)
                .NotEmpty()
                .WithMessage(m => $"A Data de Nascimento do usuário deve ser informada");
        }
    }
}
