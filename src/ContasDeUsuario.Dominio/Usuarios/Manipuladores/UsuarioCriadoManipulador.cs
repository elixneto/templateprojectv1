﻿using ContasDeUsuario.Dominio.Usuarios.Eventos;
using NucleoCompartilhado.Dominio.Eventos;
using NucleoCompartilhado.Dominio.Manipuladores;

namespace ContasDeUsuario.Dominio.Usuarios.Manipuladores
{
    public class UsuarioCriadoManipulador : ManipuladorDeEvento<UsuarioCriadoEvento>
    {
        public UsuarioCriadoManipulador(IEventoRepositorio repositorio) : base(repositorio)
        {
        }

        protected override void Executar() => base.ArmazenarEvento();
    }
}
