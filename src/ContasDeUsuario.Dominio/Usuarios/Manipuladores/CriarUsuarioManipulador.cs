﻿using ContasDeUsuario.Dominio.Usuarios.Comandos;
using ContasDeUsuario.Dominio.Usuarios.Eventos;
using ContasDeUsuario.Dominio.Usuarios.ObjetosDeValor;
using ContasDeUsuario.Dominio.Usuarios.Repositorios;
using ContasDeUsuario.Dominio.Usuarios.SituacoesDoUsuario;
using MediatR;
using NucleoCompartilhado.Dominio.Manipuladores;
using NucleoCompartilhado.Dominio.Notificacoes;
using NucleoCompartilhado.Dominio.ObjetosDeValor;
using NucleoCompartilhado.Infra.Utils.Criptografia;

namespace ContasDeUsuario.Dominio.Usuarios.Manipuladores
{
    public class CriarUsuarioManipulador : Manipulador<CriarUsuarioComando, string>
    {
        private readonly IUsuarioRepositorio _repositorio;

        public CriarUsuarioManipulador(INotificador notificador, IMediator mediator, IUsuarioRepositorio repositorio) : base(notificador, mediator)
        {
            this._repositorio = repositorio;
        }

        public override string Executar(CriarUsuarioComando comando)
        {
            var senhaMD5 = CriptoMD5.TextoParaMD5(comando.Senha);
            var login = new Login(comando.Login, senhaMD5);
            var email = new Email(comando.Email);
            var situacao = new Ativo();

            var novoUsuario = new Usuario(comando.Conta, comando.Nome, login, email, situacao, comando.DataDeNascimento, comando.EhAdmin);

            base._notificador.Adicionar(login, email, novoUsuario);

            if (base._notificador.PossuiNotificacoes)
                return string.Empty;

            _repositorio.Adicionar(novoUsuario);

            base.PublicarEvento(new UsuarioCriadoEvento(novoUsuario.Id, novoUsuario.Nome, novoUsuario.Login.Descricao));
            return novoUsuario.Id;
        }

        protected override void Validar(CriarUsuarioComando comando)
        {
            if(string.IsNullOrEmpty(comando.Senha))
                base._notificador.Adicionar($"A Senha deve ser informada");

            var usuarioJaExiste = _repositorio.ObterPorLogin(comando.Login) != null;
            if (usuarioJaExiste)
                base._notificador.Adicionar($"Já existe um usuário com o login '{comando.Login}' cadastrado");
        }
    }
}
