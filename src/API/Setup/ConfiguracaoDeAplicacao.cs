﻿using Microsoft.AspNetCore.Builder;

namespace API.Setup
{
    public class ConfiguracaoDeAplicacao
    {
        public ConfiguracaoDeAplicacao(IApplicationBuilder app)
        {
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
