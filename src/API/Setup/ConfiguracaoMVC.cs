﻿using API.Filtros;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace API.Setup
{
    public class ConfiguracaoMVC
    {
        public ConfiguracaoMVC(IServiceCollection services)
        {
            services.AddMvc(mvc =>
                {
                    mvc.Filters.Add<NotificacaoFiltro>();
                    mvc.Filters.Add<SessaoRavenDBFiltro>();
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }
    }
}
