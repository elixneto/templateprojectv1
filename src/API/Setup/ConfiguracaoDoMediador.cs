﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;

namespace API.Setup
{
    public class ConfiguracaoDoMediador
    {
        public ConfiguracaoDoMediador(IServiceCollection services)
        {
            var projeto = AppDomain.CurrentDomain;

            Assembly[] assembly = new Assembly[] {
                projeto.Load("ContasDeUsuario.Dominio")
            };

            services.AddMediatR(assembly);
        }
    }
}
