﻿using Microsoft.Extensions.DependencyInjection;
using NucleoCompartilhado.Infra.InjecaoDeDependencia;

namespace API.Setup
{
    public class InjetoresDeDependencia
    {
        public InjetoresDeDependencia(IServiceCollection services)
        {
            AplicacaoInjetor.Registrar(services);
            DominioInjetor.Registrar(services);
            RepositorioInjetor.Registrar(services);
            RepositorioDoRavenDBInjetor.Registrar(services);
            ConsultaInjetor.Registrar(services);
        }
    }
}
