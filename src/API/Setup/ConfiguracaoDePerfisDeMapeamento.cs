﻿using AutoMapper;
using ContasDeUsuario.Aplicacao.Contas;
using ContasDeUsuario.Aplicacao.Usuarios;
using Microsoft.Extensions.DependencyInjection;

namespace API.Setup
{
    public class ConfiguracaoDePerfisDeMapeamento
    {
        public ConfiguracaoDePerfisDeMapeamento(IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ContaPerfil());
                mc.AddProfile(new UsuarioPerfil());
            });

            services.AddSingleton(mappingConfig.CreateMapper());
        }
    }
}
