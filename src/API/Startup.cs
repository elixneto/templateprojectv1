﻿using API.Setup;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            new ConfiguracaoMVC(services);
            new ConfiguracaoDePerfisDeMapeamento(services);
            new ConfiguracaoDoMediador(services);
            new InjetoresDeDependencia(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            new ConfiguracaoDeAmbiente(app, env);
            new ConfiguracaoDeAplicacao(app);
        }
    }
}
