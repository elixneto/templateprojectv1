﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using NucleoCompartilhado.Dominio.Notificacoes;
using System.Net;
using System.Threading.Tasks;

namespace API.Filtros
{
    public class NotificacaoFiltro : IAsyncResultFilter
    {
        private readonly INotificador _notificador;

        public NotificacaoFiltro(INotificador notificador)
        {
            _notificador = notificador;
        }

        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if (_notificador.PossuiNotificacoes)
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.HttpContext.Response.ContentType = "application/json";

                var notificacoes = JsonConvert.SerializeObject(_notificador.Notificacoes);
                await context.HttpContext.Response.WriteAsync(notificacoes);

                return;
            }

            await next();
        }
    }
}
