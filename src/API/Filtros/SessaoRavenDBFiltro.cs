﻿using Microsoft.AspNetCore.Mvc.Filters;
using NucleoCompartilhado.Dominio.Notificacoes;
using NucleoCompartilhado.Repositorio.RavenDB;
using System.Threading.Tasks;

namespace API.Filtros
{
    public class SessaoRavenDBFiltro : IAsyncResultFilter
    {
        private readonly INotificador _notificador;
        private readonly ContextoRavenDB _contexto;

        public SessaoRavenDBFiltro(INotificador notificador, ContextoRavenDB contexto)
        {
            _notificador = notificador;
            _contexto = contexto;
        }

        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if (!_notificador.PossuiNotificacoes)
                _contexto.Commit();
            else
                _contexto.Rollback();

            await next();
        }
    }
}
