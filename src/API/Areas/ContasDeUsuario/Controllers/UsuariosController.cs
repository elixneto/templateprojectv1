﻿using ContasDeUsuario.Aplicacao.Usuarios.Interfaces;
using ContasDeUsuario.Aplicacao.Usuarios.ModelosDeVisualizacao;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Areas.ContasDeUsuario.Controllers
{
    [Route("api/contasdeusuario/[controller]")]
    public class UsuariosController : Controller
    {
        private readonly IUsuarioServico _servico;

        public UsuariosController(IUsuarioServico servico)
        {
            this._servico = servico;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CriarUsuarioMV criarUsuarioMV)
        {
            if (criarUsuarioMV == null)
                return BadRequest();

            var usuarioCriado = await _servico.Criar(criarUsuarioMV);

            return Ok(usuarioCriado);
        }
    }
}