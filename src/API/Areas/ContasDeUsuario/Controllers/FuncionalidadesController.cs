﻿using Microsoft.AspNetCore.Mvc;

namespace API.Areas.ContasDeUsuario.Controllers
{
    [Route("api/contasdeusuario/[controller]")]
    public class FuncionalidadesController : Controller
    {
        public IActionResult Get()
        {
            return Ok();
        }
    }
}