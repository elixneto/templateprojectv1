﻿using ContasDeUsuario.Aplicacao.Contas.Interfaces;
using ContasDeUsuario.Aplicacao.Contas.ModelosDeVisualizacao;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Areas.ContasDeUsuario.Controllers
{
    [Route("api/contasdeusuario/[controller]")]
    public class ContasController : Controller
    {
        private readonly IContaServico _servico;
        private readonly IContaConsulta _consulta;

        public ContasController(IContaServico servico, IContaConsulta consulta)
        {
            this._servico = servico;
            this._consulta = consulta;
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_consulta.ObterLista(1, 10));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CriarContaMV criarContaMV)
        {
            if (criarContaMV == null)
                return BadRequest();

            var resultado = await _servico.Criar(criarContaMV);

            return Ok(resultado);
        }
    }
}
