﻿using ContasDeUsuario.Dominio.Permissoes;
using ContasDeUsuario.Dominio.Permissoes.Repositorios;
using NucleoCompartilhado.Repositorio.RavenDB;

namespace ContasDeUsuario.Infra.Repositorio
{
    public class FuncionalidadeRepositorio : RepositorioRavenDB<Funcionalidade>, IFuncionalidadeRepositorio
    {
        public FuncionalidadeRepositorio(ContextoRavenDB context) : base(context) { }
    }
}
