﻿using ContasDeUsuario.Dominio.Contas;
using ContasDeUsuario.Dominio.Contas.Repositorios;
using NucleoCompartilhado.Repositorio.RavenDB;
using System.Linq;

namespace ContasDeUsuario.Infra.Repositorio
{
    public class ContaRepositorio : RepositorioRavenDB<Conta>, IContaRepositorio
    {
        public ContaRepositorio(ContextoRavenDB contexto) : base(contexto) { }

        public Conta ObterPorNome(string nome)
        {
            var resultado = base._contexto.Sessao.Query<Conta>()
                .Where(w => w.Nome.Equals(nome))
                .SingleOrDefault();

            return resultado;
        }
    }
}
