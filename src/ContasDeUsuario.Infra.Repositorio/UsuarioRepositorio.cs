﻿using ContasDeUsuario.Dominio.Usuarios;
using ContasDeUsuario.Dominio.Usuarios.Repositorios;
using NucleoCompartilhado.Repositorio.RavenDB;
using System.Linq;

namespace ContasDeUsuario.Infra.Repositorio
{
    public class UsuarioRepositorio : RepositorioRavenDB<Usuario>, IUsuarioRepositorio
    {
        public UsuarioRepositorio(ContextoRavenDB contexto) : base(contexto) { }

        public Usuario ObterPorLogin(string login)
        {
            var resultado = base._contexto.Sessao.Query<Usuario>()
                .Where(w => w.Login.Descricao.Equals(login))
                .SingleOrDefault();

            return resultado;
        }
    }
}
