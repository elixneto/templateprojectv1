﻿using System.Security.Cryptography;
using System.Text;

namespace NucleoCompartilhado.Infra.Utils.Criptografia
{
    public static class CriptoMD5
    {
        public static string TextoParaMD5(string texto)
        {
            MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(texto));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
                sBuilder.Append(data[i].ToString("x2"));

            return sBuilder.ToString();
        }
    }
}
