﻿using AutoMapper;
using ContasDeUsuario.Aplicacao.Contas.Interfaces;
using ContasDeUsuario.Aplicacao.Contas.ModelosDeVisualizacao;
using ContasDeUsuario.Aplicacao.Usuarios.ModelosDeVisualizacao;
using ContasDeUsuario.Dominio.Contas;
using ContasDeUsuario.Dominio.Usuarios;
using NucleoCompartilhado.Repositorio.RavenDB;
using System.Collections.Generic;
using System.Linq;

namespace ContasDeUsuario.Infra.Consulta
{
    public class ContaConsulta : IContaConsulta
    {
        private readonly ContextoRavenDB _contexto;
        private readonly IMapper _mapeador;

        public ContaConsulta(ContextoRavenDB contexto, IMapper mapeador)
        {
            this._contexto = contexto;
            this._mapeador = mapeador;
        }

        public List<ContaMV> ObterLista(ushort pagina, ushort quantidadeDeRegistros)
        {
            var contas = _contexto.Sessao.Query<Conta>()
                            .Skip((quantidadeDeRegistros * pagina) - quantidadeDeRegistros)
                            .Take(quantidadeDeRegistros)
                            .ToList();

            List<ContaMV> lista = new List<ContaMV>();
            foreach (var conta in contas)
            {
                var contaMV = _mapeador.Map<ContaMV>(conta);

                var usuarios = _contexto.Sessao.Query<Usuario>()
                                .Where(w => w.Conta == conta.Id)
                                .ToList();
                foreach (var usuario in usuarios)
                    contaMV.Usuarios.Add(_mapeador.Map<UsuarioMV>(usuario));

                lista.Add(contaMV);
            }

            return lista;
        }

        public ContaMV ObterPorId(string id)
        {
            throw new System.NotImplementedException();
        }
    }
}
