﻿using ContasDeUsuario.Dominio.Contas.Repositorios;
using ContasDeUsuario.Dominio.Usuarios.Repositorios;
using ContasDeUsuario.Infra.Repositorio;
using Microsoft.Extensions.DependencyInjection;
using NucleoCompartilhado.Dominio.Eventos;
using NucleoCompartilhado.Infra.Repositorio.RavenDB;

namespace NucleoCompartilhado.Infra.InjecaoDeDependencia
{
    public static class RepositorioInjetor
    {
        public static void Registrar(IServiceCollection servicos)
        {
            servicos.AddScoped<IEventoRepositorio, EventoRepositorioRavenDB>();

            servicos.AddScoped<IContaRepositorio, ContaRepositorio>();
            servicos.AddScoped<IUsuarioRepositorio, UsuarioRepositorio>();
        }
    }
}
