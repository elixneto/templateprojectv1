﻿using Microsoft.Extensions.DependencyInjection;
using NucleoCompartilhado.Infra.Repositorio.RavenDB;
using NucleoCompartilhado.Repositorio.RavenDB;
using Raven.Client.Documents;

namespace NucleoCompartilhado.Infra.InjecaoDeDependencia
{
    public static class RepositorioDoRavenDBInjetor
    {
        public static void Registrar(IServiceCollection servicos)
        {
            servicos.AddSingleton<IDocumentStore>(new ConfiguracaoDoRavenDB().DocumentStore);

            servicos.AddScoped<ContextoRavenDB>();
        }
    }
}
