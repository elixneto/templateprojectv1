﻿using ContasDeUsuario.Aplicacao.Contas.Interfaces;
using ContasDeUsuario.Infra.Consulta;
using Microsoft.Extensions.DependencyInjection;

namespace NucleoCompartilhado.Infra.InjecaoDeDependencia
{
    public static class ConsultaInjetor
    {
        public static void Registrar(IServiceCollection servicos)
        {
            servicos.AddScoped<IContaConsulta, ContaConsulta>();
        }
    }
}
