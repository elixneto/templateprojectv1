﻿using Microsoft.Extensions.DependencyInjection;
using NucleoCompartilhado.Dominio.Notificacoes;

namespace NucleoCompartilhado.Infra.InjecaoDeDependencia
{
    public static class DominioInjetor
    {
        public static void Registrar(IServiceCollection servicos)
        {
            servicos.AddScoped<INotificador, Notificador>();
        }
    }
}
