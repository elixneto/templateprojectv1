﻿using ContasDeUsuario.Aplicacao.Contas;
using ContasDeUsuario.Aplicacao.Contas.Interfaces;
using ContasDeUsuario.Aplicacao.Usuarios;
using ContasDeUsuario.Aplicacao.Usuarios.Interfaces;
using ContasDeUsuario.Infra.Consulta;
using Microsoft.Extensions.DependencyInjection;

namespace NucleoCompartilhado.Infra.InjecaoDeDependencia
{
    public static class AplicacaoInjetor
    {
        public static void Registrar(IServiceCollection servicos)
        {
            servicos.AddScoped<IContaServico, ContaServico>();
            servicos.AddScoped<IUsuarioServico, UsuarioServico>();
        }
    }
}
