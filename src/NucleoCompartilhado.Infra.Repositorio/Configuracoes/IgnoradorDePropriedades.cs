﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace NucleoCompartilhado.Infra.Repositorio.Configuracoes
{
    public class IgnoradorDePropriedades : DefaultContractResolver
    {
        private readonly string[] _propriedadesIgnoradas;

        public IgnoradorDePropriedades(string[] propriedadesIgnoradas)
        {
            this._propriedadesIgnoradas = propriedadesIgnoradas;
        }

        protected override List<MemberInfo> GetSerializableMembers(Type objectType)
        {
            var members = base.GetSerializableMembers(objectType);
            foreach (var propriedade in this._propriedadesIgnoradas)
                members.RemoveAll(x => x.Name == propriedade);

            return members;
        }
    }
}
