﻿namespace NucleoCompartilhado.Infra.Repositorio.Configuracoes
{
    public class ConfiguradorDeBancoDeDados
    {
        public string Url { get; set; }
        public string BancoDeDados { get; set; }

        public ConfiguradorDeBancoDeDados(string url, string bancoDeDados)
        {
            this.Url = url;
            this.BancoDeDados = bancoDeDados;
        }
    }
}
