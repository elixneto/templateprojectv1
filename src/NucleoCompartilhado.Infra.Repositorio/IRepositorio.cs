﻿using NucleoCompartilhado.Dominio.Entidades;
using System.Collections.Generic;

namespace NucleoCompartilhado.Infra.Repositorio
{
    public interface IRepositorio<T> where T : Entidade
    {
        T ObterPorId(string id);
        void Adicionar(T entidade);
        void Adicionar(ICollection<T> entidades);
        void Atualizar(T entidade);
        void Remover(T entidade);
        void Remover(ICollection<T> entidades);
    }
}
