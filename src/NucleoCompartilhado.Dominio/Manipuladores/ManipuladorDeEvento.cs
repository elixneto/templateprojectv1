﻿using MediatR;
using NucleoCompartilhado.Dominio.Eventos;
using System.Threading;
using System.Threading.Tasks;

namespace NucleoCompartilhado.Dominio.Manipuladores
{
    public abstract class ManipuladorDeEvento<T> : INotificationHandler<T> where T : Evento
    {
        private readonly IEventoRepositorio _repositorio;
        public T Evento;

        protected abstract void Executar();

        public ManipuladorDeEvento(IEventoRepositorio repositorio)
        {
            this._repositorio = repositorio;
        }

        public Task Handle(T evento, CancellationToken cancellationToken)
        {
            this.Evento = evento;
            this.Executar();
            return Task.CompletedTask;
        }

        public void ArmazenarEvento()
        {
            _repositorio.Armazenar(this.Evento);
        }
    }
}
