﻿using MediatR;
using NucleoCompartilhado.Dominio.Comandos;
using NucleoCompartilhado.Dominio.Eventos;
using NucleoCompartilhado.Dominio.Notificacoes;
using System.Threading;
using System.Threading.Tasks;

namespace NucleoCompartilhado.Dominio.Manipuladores
{
    public abstract class Manipulador<TRequest, TResponse> : IRequestHandler<TRequest, TResponse>
        where TRequest : Comando<TResponse>
    {
        protected readonly INotificador _notificador;
        protected readonly IMediator _mediator;

        public Manipulador(INotificador notificador, IMediator mediator) { this._notificador = notificador; this._mediator = mediator; }

        public abstract TResponse Executar(TRequest comando);
        protected abstract void Validar(TRequest request);

        public void PublicarEvento(Evento evento) => this._mediator.Publish(evento);

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken)
        {
            this.Validar(request);
            if (!this._notificador.PossuiNotificacoes)
                return this.Executar(request);

            return default;
        }
    }
}
