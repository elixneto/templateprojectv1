﻿using NucleoCompartilhado.Dominio.Notificacoes;
using System;

namespace NucleoCompartilhado.Dominio.Entidades
{
    public class Entidade : Notificavel
    {
        public string Id { get; private set; }

        public Entidade()
        {
            this.Id = this.Id ?? Guid.NewGuid().ToString();
        }
    }
}
