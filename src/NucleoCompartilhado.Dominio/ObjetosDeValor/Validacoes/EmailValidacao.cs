﻿using FluentValidation;

namespace NucleoCompartilhado.Dominio.ObjetosDeValor.Validacoes
{
    public class EmailValidacao : AbstractValidator<Email>
    {
        public EmailValidacao()
        {
            RuleFor(r => r.Endereco)
                .NotEmpty()
                .WithMessage(m => $"O Endereço de Email deve ser informado")
                .EmailAddress()
                .WithMessage(m => $"O Endereço de Email '{m.Endereco}' não é válido");
        }
    }
}
