﻿using NucleoCompartilhado.Dominio.Notificacoes;
using System;

namespace NucleoCompartilhado.Dominio.ObjetosDeValor
{
    public abstract class ObjetoDeValor<T> : Notificavel, IEquatable<T> where T : ObjetoDeValor<T>
    {
        public override bool Equals(object objeto)
        {
            return this.Equals(objeto as T);
        }
        public virtual bool Equals(T outroObjeto)
        {
            if (ReferenceEquals(null, outroObjeto)) return false;
            if (ReferenceEquals(this, outroObjeto)) return true;
            return PropriedadesIguais(outroObjeto);
        }

        public static bool operator ==(ObjetoDeValor<T> a, ObjetoDeValor<T> b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }
        public static bool operator !=(ObjetoDeValor<T> a, ObjetoDeValor<T> b)
        {
            return !(a == b);
        }

        public abstract bool PropriedadesIguais(T outroObjeto);
    }
}
