﻿using NucleoCompartilhado.Dominio.ObjetosDeValor.Validacoes;

namespace NucleoCompartilhado.Dominio.ObjetosDeValor
{
    public class Email : ObjetoDeValor<Email>
    {
        public string Endereco { get; private set; }

        public Email(string endereco)
        {
            this.Endereco = endereco;

            base.Validar(this, new EmailValidacao());
        }

        public override bool PropriedadesIguais(Email outroObjeto)
        {
            return this.Endereco == outroObjeto.Endereco;
        }
    }
}
