﻿using MediatR;
using System;

namespace NucleoCompartilhado.Dominio.Eventos
{
    public class Evento : INotification
    {
        public string Nome { get; set; }
        public DateTime DataDeCriacao => DateTime.Now;
        public virtual string Mensagem { get; set; }
    }
}
