﻿namespace NucleoCompartilhado.Dominio.Eventos
{
    public interface IEventoRepositorio
    {
        void Armazenar(Evento evento);
    }
}
