﻿using MediatR;

namespace NucleoCompartilhado.Dominio.Comandos
{
    public abstract class Comando<T> : IRequest<T>
    {
    }
}
