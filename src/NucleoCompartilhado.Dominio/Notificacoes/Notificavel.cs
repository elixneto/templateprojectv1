﻿using FluentValidation;
using System.Collections.Generic;
using System.Linq;

namespace NucleoCompartilhado.Dominio.Notificacoes
{
    public abstract class Notificavel
    {
        public bool EhValido => !this._notificacoes.Any();
        private readonly IList<string> _notificacoes = new List<string>();
        public IReadOnlyList<string> Notificacoes => this._notificacoes.ToArray();

        public void Validar<T>(T entidade, AbstractValidator<T> validator)
        {
            var validationResult = validator.Validate(entidade);
            foreach (var error in validationResult.Errors)
                this._notificacoes.Add(error.ErrorMessage);
        }

        protected void AdicionarNotificacao(string notificacao) => this._notificacoes.Add(notificacao);
        protected void AdicionarNotificacoes(ICollection<string> notificacoes)
        {
            foreach (var notificacao in notificacoes)
                this.AdicionarNotificacao(notificacao);
        }
    }
}
