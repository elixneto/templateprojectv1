﻿using CollectionMapper.RavenDB.NetCore;
using ContasDeUsuario.Dominio.Contas;
using ContasDeUsuario.Dominio.Usuarios;
using NucleoCompartilhado.Dominio.Eventos;

namespace NucleoCompartilhado.Infra.Repositorio.RavenDB
{
    public class MapeadorDoRavenDB : CollectionMapperRavenDB
    {
        public MapeadorDoRavenDB()
        {
            Map<Evento>("Eventos");
            Map<Conta>("Contas");
            Map<Usuario>("Usuarios");
        }
    }
}
