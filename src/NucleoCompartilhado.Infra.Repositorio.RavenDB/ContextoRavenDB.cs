﻿using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace NucleoCompartilhado.Repositorio.RavenDB
{
    public class ContextoRavenDB
    {
        public readonly IDocumentSession Sessao;

        public ContextoRavenDB(IDocumentStore documentStore)
        {
            this.Sessao = documentStore.OpenSession();
        }

        public void Commit()
        {
            this.Sessao.SaveChanges();
        }

        public void Rollback()
        {
            this.Sessao.Dispose();
        }
    }
}
