﻿using NucleoCompartilhado.Dominio.Entidades;
using NucleoCompartilhado.Infra.Repositorio;
using System;
using System.Collections.Generic;

namespace NucleoCompartilhado.Repositorio.RavenDB
{
    public abstract class RepositorioRavenDB<T> : IRepositorio<T> where T : Entidade
    {
        public readonly ContextoRavenDB _contexto;

        public RepositorioRavenDB(ContextoRavenDB context)
        {
            this._contexto = context;
        }

        public void Adicionar(T entidade) => _contexto.Sessao.Store(entidade, entidade.Id);

        public void Adicionar(ICollection<T> entidades)
        {
            foreach (var entidade in entidades)
                this.Adicionar(entidade);
        }

        public void Atualizar(T entidade)
        {
            throw new NotImplementedException();
        }

        public T ObterPorId(string id) => _contexto.Sessao.Load<T>(id);

        public void Remover(T entidade) => _contexto.Sessao.Delete(entidade);

        public void Remover(ICollection<T> entidades)
        {
            foreach (var entidade in entidades)
                this.Remover(entidade);
        }
    }
}
