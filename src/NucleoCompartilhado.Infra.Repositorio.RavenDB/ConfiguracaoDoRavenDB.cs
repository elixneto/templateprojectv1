﻿using NucleoCompartilhado.Infra.Repositorio.Configuracoes;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations.Revisions;
using System.Collections.Generic;

namespace NucleoCompartilhado.Infra.Repositorio.RavenDB
{
    public class ConfiguracaoDoRavenDB
    {
        private readonly MapeadorDoRavenDB mapeadorDeColecoes = new MapeadorDoRavenDB();
        private ConfiguradorDeBancoDeDados _configuradorDeBancoDeDados { get; }
        private IgnoradorDePropriedades _ignoradorDePropriedades { get; }
        public DocumentStore DocumentStore { get; }

        public ConfiguracaoDoRavenDB()
        {
            _configuradorDeBancoDeDados = new ConfiguradorDeBancoDeDados("http://127.0.0.1:15379", "starterDDD");

            _ignoradorDePropriedades = new IgnoradorDePropriedades(new string[] { "EhValido", "Notificacoes" });

            DocumentStore = new DocumentStore()
            {
                Urls = new string[] { _configuradorDeBancoDeDados.Url },
                Database = _configuradorDeBancoDeDados.BancoDeDados,
                Conventions = {
                    JsonContractResolver = _ignoradorDePropriedades,
                    FindCollectionName = (type) => mapeadorDeColecoes.FindCollectionBy(type)
                }
            };
            DocumentStore.Initialize();

            ConfigurarRevisoes(DocumentStore, mapeadorDeColecoes);
        }

        private void ConfigurarRevisoes(DocumentStore documentStore, MapeadorDoRavenDB mapeadorDeColecoes)
        {
            var configuracaoDeRevisoes = new RevisionsConfiguration() { Collections = new Dictionary<string, RevisionsCollectionConfiguration>() };
            foreach (var colecao in mapeadorDeColecoes.Collections)
                configuracaoDeRevisoes.Collections.Add(colecao.CollectionName, new RevisionsCollectionConfiguration() { Disabled = false, MinimumRevisionsToKeep = 10 });
            documentStore.Maintenance.Send(new ConfigureRevisionsOperation(configuracaoDeRevisoes));
        }
    }
}
