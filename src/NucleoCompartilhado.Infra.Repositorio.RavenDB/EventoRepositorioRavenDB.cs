﻿using NucleoCompartilhado.Dominio.Eventos;
using NucleoCompartilhado.Repositorio.RavenDB;

namespace NucleoCompartilhado.Infra.Repositorio.RavenDB
{
    public class EventoRepositorioRavenDB : IEventoRepositorio
    {
        private readonly ContextoRavenDB _contexto;

        public EventoRepositorioRavenDB(ContextoRavenDB contexto)
        {
            _contexto = contexto;
        }

        public void Armazenar(Evento evento)
        {
            Evento novoEvento = new Evento();
            novoEvento.Nome = evento.GetType().Name;
            novoEvento.Mensagem = evento.Mensagem;

            _contexto.Sessao.Store(novoEvento);
        }
    }
}
